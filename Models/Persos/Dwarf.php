<?php 
    class Dwarf extends Character 
    {


       public function __construct($name, $role){

            /**
             * Appel au constructeur parent afin d'executer le code trait racial
             */
            parent::__construct($name, $role);
            $this->force *= 1.25; 
        }
    }
    

?>
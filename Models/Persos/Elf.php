<?php 
    class Elf extends Character 
    {
        public function __construct($name, $role){

            /**
             * Appel au constructeur parent afin d'executer le code trait racial
             */
            parent::__construct($name, $role);
            $this->agility *= 1.25; 
        }
    }
    

?>
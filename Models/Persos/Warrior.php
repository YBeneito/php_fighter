<?php 

    /**
     * Les classes des types pocedent des valueurs aux attributs modifié 
     */
    class Warrior 
    {
        public $force;
        public $agility;
        public $endurance;

        public function __construct(){
            $this->force = 5;
            $this->agility = -5;
            $this->endurance = 0;        
        }
    }
    
?>
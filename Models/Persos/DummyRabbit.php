<?php 

    class DummyRabbit extends Character 
    {
       
        public function __construct($name, $role){

            /**
             * Appel au constructeur parent afin d'executer le code des 25% d'hp
             */
            parent::__construct($name, $role);
            $this->health *= 0.75; 
        }
    }
    

?>
<?php 

    /**
     * Le traits des humain est d'avoir 25% de pv en plus.
     */
    class Human extends Character 
    {
        public function __construct($name, $role){

            /**
             * Appel au constructeur parent afin d'executer le code trait racial
             */
            parent::__construct($name, $role);
            $this->health *= 1.25; 
        }
    }
    


?>
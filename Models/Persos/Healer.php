<?php 

    /**
     * Les classes des types pocedent des valueurs aux attributs modifié 
     */
    class Healer 
    {
        public $force;
        public $agility;
        public $endurance;

        public function __construct(){
            $this->force = -5;
            $this->agility = 0;
            $this->endurance = 5;        
        }
    }
    
?>
<?php 
    class DDT
    {
        //mit en private pour pas que l'utilisateur puisse modifier l'arène à la vollée.
        private $arena;

        public function __construct()
        {
            $this->arena = array();
        }
            
        /**
         * On peut ajouter les combattants à notre arène en passant pas cette fonction
         */
        public function addFighter($fighter)
        {
            array_push($this->arena, $fighter);
        }

        /**
         * Ou encore en ajoutant plusieurs d'un coup
         */

        public function addFighters($fighters)
        {
            $this->arena = array_merge($this->arena, $fighters);
        }


        /**
         * Les tours de jeux sont décrit ici
        */
        
        private function actions(){
            //choisir attaquant et def
            $this->chooseFighters();

            //effectuer l'attaque
            $this->att->attack($this->def);

            //nettoyer si besoin
            $this->clean($this->def);
        }

        /**
        * Ici on choisis un attaquant et un défenseur
        */
        public function chooseFighters() {
            $this->att = $this->arena[rand(0, count($this->arena)-1)];
            do {
                $this->def = $this->arena[rand(0, count($this->arena)-1)];
            } while ($this->att == $this->def);
        }
        
        /**
         *  Si un perso meurt, on le retire de l'arène 
         */
        private function clean($def){ 
            if($def->health <= 0){
                $defindex = array_search($def,$this->arena);
                array_splice($this->arena, $defindex ,1);
                echo '<h1 style="color: red">'. $def->name .' est mort ... RIP!!</h1>';
            }
        }

        /**
         * On peut lancer la baston ici tant qu'il reste des persos dans l'arène 
         * les actions sont relancées.
         */
        public function launchFight(){
            echo "<h1 style='color: blue'> TING TING TING Les combatants prennent place le combat peut commencer !!!</h1>";
            while(count($this->arena)>1){
                $this->actions();
            }
            if(count($this->arena) == 1){
                echo '<h1 style="color: green">'. $this->arena[0]->name .' est notre grand gagnant à lui la gloire </h1>';
                
            }
            
        }

        /**
         * Cette fonction crée un tableau des combatants avec leurs infos
         */
        public function showArena(){
            echo "<table> <tablehead> <th>NOM</th> <th>PV</th> <th>FORCE</th> <th>ENDU</th> <th>AGI</th> </tablehead>";
            foreach ($this->arena as $char => $val) {
            echo "<tr><td style='border: 1px solid black'>".$val->name."</td> <td style='border: 1px solid black'>".$val->health."</td><td style='border: 1px solid black'>".$val->force."</td><td style='border: 1px solid black'>".$val->endurance."</td><td style='border: 1px solid black'>".$val->agility."</td> </tr>";
            }
            echo "</table>";
        }
    }

?>
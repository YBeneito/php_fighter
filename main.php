<audio src="./sound/Guile Theme.mp3" autoplay controls></audio>
<?php 

/**
 * Récupération des fichiers à inclure
 */
$folders = scandir("./Models/Persos/");
foreach ($folders as $file) {
   $path = './Models/Persos/'.$file;
   if(is_file($path)) include_once($path); 
};

include_once("./Models/Arene/DDT.php");

$area = new DDT();

$p1 = new Elf("p1", new Warrior());

$p2 = new Human("p2", new Archer());

$p3 = new Dwarf("p3", new Healer());

$p4 = new Elf("p4", new Archer());

$p5 = new Dwarf("p5", new Warrior());

$p6 = new Dwarf("p6", new Archer());

$p7 = new Elf("p7", new Archer());

$p8 = new Human("p8", new Warrior());

$p9 = new Human("p9", new Healer());

$fighters = [$p1,$p2,$p3,$p4,$p5,$p6,$p7,$p8,$p9];

$area->addFighters($fighters);

// $area->addFighter($p1);
// $area->addFighter($p2);
// $area->addFighter($p3);
// $area->addFighter($p4);
// $area->addFighter($p5);
// $area->addFighter($p6);
// $area->addFighter($p7);
// $area->addFighter($p8);
// $area->addFighter($p9);

$area->showArena();

// echo "<pre>";
// var_dump($area);
// echo "</pre>";

$area->launchFight();


/**
 * 2 hommes entrent un seul sort !! 
 * Mettre deux combatants dans une liste nommée DOM_DU_TONNERRE
 * 
 * Faire en sorte que:
 *  Les combatants tapent chacun leur tours jusque mort s'en suive /1commit
 *  Les combatant tapent de manière aleatoire jusque mort s'en suive /1commit
 * 
 * Tips:
 *  Les persos possèdent une methode d'attaque qui prends en arg la cible de l'attaque
 *  Le calcul des dégats est un truc du genre PV -= FOR - ENDU * 0.25 time()² % 3
 *  Afficher en console des log de type:
 *      ATT_NAME a attaqué DEF_NAME pour XX degats
 *      DEF_NAME est à l'article de la mort, il lui reste DEF_PV 
 */

// function displayChar($area){    
//     foreach ($area as $player) {
//         echo "<pre>";
//         print_r($player);
//         echo "</pre>";
//     }
// }
// displayChar($area);

?>






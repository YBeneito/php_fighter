<?php 
    //La Classe Charactere servira à instancier nos personnages
    /**
     * Sauf pour l'endurance les valeurs max de chaques attributs sont de 50 + les traits raciaux/rôle.
     * Ajuster la valeur max de l'endu pour avoir des combats pas trop longs ni trop courts.
     * À la creation d'un perso ses attributs pricipaux sont tirés aleatoirement, avec une valeur totale max entre 30 et 55.
     */
    class Character 
    {
        public $force;
        public $agility;
        public $endurance;
        public $health;
        public $name;
        public $role;

        public function __construct($name, $role){
            $this->generateValues();
            $this->health = 100;
            $this->role = $role;
            //check les conditions si -=0
            $this->force += $this->role->force;
            $this->agility += $this->role->agility;
            $this->endurance += $this->role->endurance;

            $this->name = $name;
        }

        /**
         * Renvoies un nb aleatoire entre 0 et 50 
         * On l'utilise que que à la creation de "character" dans le constructeur du coup on la mets en private 
         */
        private function generateValues(){
           $ok = false;
            do {
                $this->force = rand(30,50);
                $this->agility = rand(30,50);
                $this->endurance = rand(20,30);                 
                $total = $this->force + $this->agility + $this->endurance;
                if($total > 50 && $total < 100){
                    $ok = true;
                }
            } while ($ok = false);
        }

        // Fonction d'attaque simple + jet de dès pour 25% dég crit et 20% chance crit 
        public function attack($cible){
            $crit = 1.25;
            $rand = rand(0,10);
         
            $degats =  $this->force - $cible->endurance;
            if ($rand >8){
                $degats *= $crit;
                echo "critical hit!!! ";   
            }
            $cible->health -= $degats;
            echo $this->name . " attaque " . $cible->name ."\n en lui faisant " . $degats . " points de dégats. Il reste donc ". $cible->health .  " PV. <br><br>";
        }
    }

?>